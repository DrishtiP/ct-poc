// import { NgxMoveableModule } from 'ngx-moveable';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { WellListComponent } from './well-list/well-list.component';
import { WellComponent } from './well/well.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropComponent } from './drag-drop/drag-drop.component';
import { ButtonComponent } from './drag-drop/button/button.component';
import { CheckboxComponent } from './drag-drop/checkbox/checkbox.component';
import { DropdownComponent } from './drag-drop/dropdown/dropdown.component';
import { TextboxComponent } from './drag-drop/textbox/textbox.component';
// import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    WellListComponent,
    WellComponent,
    DragDropComponent,
    ButtonComponent,
    CheckboxComponent,
    DropdownComponent,
    TextboxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    // DragDropModule,
    // NgxMoveableModule
  ],
  providers: [],
  entryComponents: [ButtonComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
