import { TextboxComponent } from './textbox/textbox.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { ButtonComponent } from './button/button.component';
import { Component, ComponentFactory, ComponentFactoryResolver, ComponentRef, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { NgxMoveableComponent } from "ngx-moveable";
import { CheckboxComponent } from './checkbox/checkbox.component';

@Component({
  selector: 'app-drag-drop',
  templateUrl: './drag-drop.component.html',
  styleUrls: ['./drag-drop.component.scss']
})
export class DragDropComponent implements OnInit {
  droppedElement = 'DROP ELEMENT HERE'
  @ViewChild("displayArea", { read: ViewContainerRef }) container: ViewContainerRef;
  draggedData: any;

  constructor(private resolver: ComponentFactoryResolver, private viewContainerRef: ViewContainerRef) { }
  dragList = [
    { title: 'checkbox', id: 'CHECKBOX', comp: CheckboxComponent },
    { title: 'Dropdown', id: 'DROPDOWN', comp: DropdownComponent },
    { title: 'TextBox', id: 'TEXTBOX', comp: TextboxComponent },
    { title: 'Button', id: 'BUTTON', comp: ButtonComponent }]
  ngOnInit(): void {
  }
  drop(e) {
    this.droppedElement = this.draggedData.id;
    const componentFactory = this.resolver.resolveComponentFactory(this.draggedData.comp);
    const containerRef = this.viewContainerRef;
    this.container.clear();
    this.container.createComponent(componentFactory);
  }
  dragEnd(e) {
    e.preventDefault();
    // console.log("dragEnd", e);
  }
  drag(e, data) {
    this.draggedData = data;
  }
}
