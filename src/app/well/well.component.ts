import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-well',
  templateUrl: './well.component.html',
  styleUrls: ['./well.component.scss']
})
export class WellComponent implements OnInit {

  @Input() wellInfo: any;
  wellForm;
  @Output() formData = new EventEmitter;
  @Output() closeForm = new EventEmitter;
  constructor() { }

  ngOnInit(): void {
    this.wellForm = new FormGroup({
      name: new FormControl(this.wellInfo ? this.wellInfo.name : '', Validators.required),
      type: new FormControl(this.wellInfo ? this.wellInfo.type : '', Validators.required),
      sourceKey: new FormControl({ value: this.wellInfo ? this.wellInfo.sourceKey : '', disabled: this.wellInfo.source ? true : false }, Validators.required)
    })
  }
  submitForm() {
    this.formData.emit(this.wellForm.value);
  }
  backToList() {
    this.closeForm.emit(false);
  }
}
