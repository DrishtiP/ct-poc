import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  userList = [{ name: 'Mahesh', age: 20 }, { name: 'Shakti', age: 12 }, { name: 'Krishna', age: 55 }, { name: 'Radha', age: 40 }]
  show = false;
  constructor() { }

  ngOnInit(): void {
  }

  showDetails() {
    this.show = true;
  }
  hideDetails() {
    this.show = false;
  }

}
