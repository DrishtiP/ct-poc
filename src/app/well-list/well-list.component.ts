import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-well-list',
  templateUrl: './well-list.component.html',
  styleUrls: ['./well-list.component.scss']
})
export class WellListComponent implements OnInit {

  wellList = [{ id: 1, name: 'Mahesh', type: 'rls', sourceKey: '10001', color: 'green' }, { id: 2, name: 'Shakti', type: 'rls', sourceKey: '10001', color: 'green' }, { id: 3, name: 'Krishna', type: 'esp', sourceKey: '10001', color: 'pink' }, { id: 4, name: 'Radha', type: 'rls', sourceKey: '10001', color: 'green' }];
  openWell = false;
  listInfo = {};

  constructor() { }

  ngOnInit(): void { }

  openForm(data) {
    this.openWell = true;
    this.listInfo = data;
  }

  updatedWellData(e) {
    this.openWell = false;
    console.log(e);
    if (Object.keys(this.listInfo).length === 0) {
      e['color'] = this.assignColor(e.type);
      e['id'] = Math.random().toString().substr(2, 5);
      this.wellList.push(e);
      console.log(this.wellList);
    }
    else {
      this.wellList.forEach((ele) => {
        if (ele.id && (ele.id === this.listInfo['id'])) {
          ele.name = e.name;
          ele.type = e.type;
          ele.color = this.assignColor(e.type);
        }
      });
    }
  }

  addNew() {
    this.openWell = true;
    this.listInfo = {};
  }

  closeForm(e) {
    this.openWell = false;
  }

  assignColor(type) {
    let color;
    this.wellList.forEach((ele) => {
      if (type === ele.type) {
        color = ele.color;
      }
      else {
        color = '#'+Math.floor(Math.random()*16777215).toString(16);;
      }
    })
    return color
  }

}
